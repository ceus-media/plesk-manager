<?php
require_once 'vendor/autoload.php';
new UI_DevOutput;


if( !file_exists( 'config.ini.inc' ) )
	die( 'Missing config file.' );
$config	= (object) parse_ini_file( 'config.ini.inc' );

$client	= new \PleskX\Api\Client( $config->hostname );
$client->setCredentials( $config->username, $config->password );

//print_m( $client->server()->getProtos() );
print_m( $client->databaseServer()->getAll() );
print_m( $client->site()->getAll() );

